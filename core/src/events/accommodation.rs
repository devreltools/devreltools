use chrono::{DateTime, Utc};
use crate::finance::Cost;

#[derive(juniper::GraphQLObject)]
pub struct Hotel {
  name: String,
}

#[derive(juniper::GraphQLObject)]
pub struct Accommodation {
  arrive: DateTime<Utc>,
  depart: DateTime<Utc>,
  cost: Cost,
  hotel: Hotel,
}
