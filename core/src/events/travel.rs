use chrono::{DateTime, Utc};
use crate::finance::Cost;

#[derive(juniper::GraphQLObject)]
pub struct Flight {
  flight_numbers: Vec<String>,
}

#[derive(juniper::GraphQLObject)]
pub struct Travel {
  arrive: DateTime<Utc>,
  depart: DateTime<Utc>,
  cost: Cost,
  /// Providing your flight information is optional, but if you provide it we can update your Slack status, send you alerts, and share the information with team mates that are also going to this event ...
  flights: Option<Vec<Flight>>,
}
