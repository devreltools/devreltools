use chrono::{DateTime, Utc};

#[derive(juniper::GraphQLEnum)]
pub enum EventType {
    Conference,
    Podcast,
    UserGroup,
    Webinar,
}

#[derive(juniper::GraphQLObject)]
pub struct Event {
    pub id: String,
    pub name: String,
    pub r#type: EventType,
    pub date_start: DateTime<Utc>,
    pub date_end: DateTime<Utc>,
}
