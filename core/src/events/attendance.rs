use crate::events::{Accommodation, Event, Travel};

#[derive(juniper::GraphQLEnum)]
pub enum Role {
  Attendee,
  Keynote,
  Presentation,
  Sponsor,
  Workshop,
}

#[derive(juniper::GraphQLObject)]
pub struct Attendance {
  event: Event,
  roles: Vec<Role>,
  travel: Option<Travel>,
  accommodation: Option<Vec<Accommodation>>,
}
