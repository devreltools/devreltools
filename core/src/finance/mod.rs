#[derive(juniper::GraphQLObject)]
pub struct Cost {
  currency: String,
  price: f64,
}
