#[derive(juniper::GraphQLEnum)]
pub enum Type {
  Language,
  Framework,
  Tool,
  Methodology,
}

#[derive(juniper::GraphQLEnum)]
pub enum Status {
  Access,
  Trial,
  Promote,
  Support,

  Deprecated,
  Abandoned,
}

#[derive(juniper::GraphQLObject)]
pub struct Technology {
  name: String,
  status: Status,
  r#type: Type,
}
