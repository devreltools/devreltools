#[macro_use]
extern crate log;

use actix::prelude::*;
use actix_web::web::Data;
use actix_web::{middleware, web, App, Error, HttpResponse, HttpServer};
use cfpland::{ConferenceResults, FetcherActor};
use juniper::http::playground::playground_source;
use juniper_actix::graphql_handler;
use std::io;
use std::{sync::Arc, time::Duration};

mod schema;
use schema::{create_schema, GraphQLContext, Schema};

mod gitops;
use gitops::GitSync;

async fn graphql(
    // The GraphQL schema
    schema: Data<Arc<Schema>>,
    cfpland: Data<Addr<FetcherActor>>,
    req: actix_web::HttpRequest,
    payload: actix_web::web::Payload,
) -> Result<HttpResponse, Error> {
    // Instantiate a context
    let ctx = GraphQLContext {
        cfpland: cfpland.get_ref().clone(),
    };

    graphql_handler(&schema, &ctx, req, payload).await
}

async fn playground() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(playground_source("/", None))
}

#[actix_rt::main]
async fn main() -> io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    // Create Juniper schema
    let schema = std::sync::Arc::new(create_schema());

    let cfpland = FetcherActor {
        refresh_every: Duration::from_secs(5),
        conferences: ConferenceResults { items: vec![] },
    }
    .start();

    let gitops = GitSync {
        git_url: String::from("git@github.com:packethost/devrel-enchiridion"),
        sync_every: Duration::from_secs(5),
    }
    .start();

    // Start http server
    HttpServer::new(move || {
        App::new()
            .data(schema.clone())
            .data(cfpland.clone())
            .wrap(middleware::Logger::default())
            .service(
                web::resource("/")
                    .route(web::post().to(graphql))
                    .route(web::get().to(playground)),
            )
    })
    .bind("127.0.0.1:8080")
    .unwrap()
    .run()
    .await
}
