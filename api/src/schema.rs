use actix::prelude::*;
use cfpland::{Conference, ConferenceResults, FetcherActor, GetConferencesCommand};
use chrono::prelude::*;
use core::events::{Event, EventType};
use juniper::{Context, EmptyMutation, EmptySubscription, FieldResult, RootNode};

pub struct GraphQLContext {
    pub cfpland: Addr<FetcherActor>,
}

impl Context for GraphQLContext {}

pub struct QueryRoot;

#[juniper::graphql_object(Context = GraphQLContext)]
impl QueryRoot {
    async fn events() -> FieldResult<Vec<Event>> {
        Ok(vec![Event {
            id: String::from("my-event"),
            name: String::from("My Event"),
            date_start: Utc::now(),
            date_end: Utc::now(),
            // Is this a pain? Should we rename this property?
            r#type: EventType::Conference,
        }])
    }

    async fn conferences(ctx: &GraphQLContext) -> FieldResult<Vec<Conference>> {
        let res: ConferenceResults = ctx.cfpland.send(GetConferencesCommand).await?;

        return Ok(res.items);
    }
}

pub type Schema =
    RootNode<'static, QueryRoot, EmptyMutation<GraphQLContext>, EmptySubscription<GraphQLContext>>;

pub fn create_schema() -> Schema {
    return Schema::new(
        QueryRoot,
        EmptyMutation::<GraphQLContext>::new(),
        EmptySubscription::<GraphQLContext>::new(),
    );
}
