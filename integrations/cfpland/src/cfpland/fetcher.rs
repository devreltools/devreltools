use crate::cfpland::ConferenceResults;

pub fn update_conferences() -> Result<ConferenceResults, minreq::Error> {
    return minreq::get("https://api.cfpland.com/v0/conferences")
        .send()?
        .json::<ConferenceResults>();
}
