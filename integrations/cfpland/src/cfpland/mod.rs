// use chrono::prelude::*;
use serde::{Deserialize, Deserializer};

mod actix_actor;
pub use crate::cfpland::actix_actor::{Fetcher as FetcherActor, GetConferencesCommand};

mod fetcher;
pub use crate::cfpland::fetcher::update_conferences;

#[derive(Clone, Debug, juniper::GraphQLEnum)]
pub enum Region {
    // Sadly a "None" string needs to be handled
    // https://github.com/cfpland/api/issues/10
    None,
    Africa,
    Americas,
    Asia,
    Europe,
    Oceania,
}

impl<'de> Deserialize<'de> for Region {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        return match Deserialize::deserialize(deserializer)? {
            // See comment on Enum declaration
            "None" => Ok(Region::None),
            "Africa" => Ok(Region::Africa),
            "Americas" => Ok(Region::Americas),
            "Asia" => Ok(Region::Asia),
            "Europe" => Ok(Region::Europe),
            "Oceania" => Ok(Region::Oceania),

            s => Err(format!("Failed to match Region enum for value {}", s))
                .map_err(serde::de::Error::custom),
        };
    }
}

#[derive(Clone, Debug, juniper::GraphQLEnum)]
pub enum Category {
    CSS,
    Data,
    Design,
    DevOps,
    DotNet,
    General,
    Go,
    Java,
    JavaScript,
    Mobile,
    PHP,
    Python,
    Ruby,
    Security,
}

impl<'de> Deserialize<'de> for Category {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        return match Deserialize::deserialize(deserializer)? {
            "CSS" => Ok(Category::CSS),
            "Data" => Ok(Category::Data),
            "Design" => Ok(Category::Design),
            "DevOps" => Ok(Category::DevOps),
            ".NET" => Ok(Category::DotNet),
            "General" => Ok(Category::General),
            "Go" => Ok(Category::Go),
            "Java" => Ok(Category::Java),
            "Javascript" => Ok(Category::JavaScript),
            "Mobile" => Ok(Category::Mobile),
            "PHP" => Ok(Category::PHP),
            "Python" => Ok(Category::Python),
            "Ruby" => Ok(Category::Ruby),
            "Security" => Ok(Category::Security),

            s => Err(format!("Failed to match Category enum for value {}", s))
                .map_err(serde::de::Error::custom),
        };
    }
}

#[derive(Clone, Deserialize, Debug, juniper::GraphQLObject)]
pub struct Conference {
    name: String,
    description: Option<String>,
    category: Option<Category>,

    event_end_date: Option<String>,
    event_start_date: Option<String>,
    event_url: Option<String>,

    cfp_start_date: String,
    cfp_due_date: String,
    cfp_url: String,

    twitter: Option<String>,
    region: Option<Region>,
    country: Option<String>,
    city: Option<String>,
    location: Option<String>,
    latitude: Option<i32>,
    longitude: Option<i32>,
}

#[derive(Deserialize, Debug, Default)]
pub struct ConferenceResults {
    pub items: Vec<Conference>,
}

// let utc: DateTime<Utc> = Utc::now();       // e.g. `2014-11-28T12:45:59.324310806Z`
// let local: DateTime<Local> = Local::now(); // e.g. `2014-11-28T21:45:59.324310806+09:00`

// export interface ConferenceDto {
//     provider: string;
//     providerId?: string;
//     subregion?: string;
//     perks_checked?: boolean;
//     perks_list?: string;
//     travel_covered?: boolean;
//     hotel_covered?: boolean;
//     stipend_covered?: boolean;
//     created_days_back?: number;
//   }
