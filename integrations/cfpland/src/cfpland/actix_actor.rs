use actix::prelude::*;
use std::time::Duration;

use crate::cfpland::fetcher::update_conferences;
use crate::cfpland::ConferenceResults;

#[derive(Default, Debug)]
pub struct Fetcher {
    pub refresh_every: Duration,
    pub conferences: ConferenceResults,
}

impl Actor for Fetcher {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Context<Self>) {
        println!("Starting! Refreshing at {:?}", self.refresh_every);

        ctx.run_interval(self.refresh_every, move |act, ctx| {
            println!("Hi");

            act.conferences = match update_conferences() {
                Ok(r) => r,

                Err(e) => {
                    println!("Failed. We'll get it during the next interval: {}", e);
                    return ();
                }
            };

            println!("DOne for now");
        });
    }

    fn stopped(&mut self, _ctx: &mut Context<Self>) {
        println!("Fetcher has stopped");
    }
}

impl ArbiterService for Fetcher {
    fn service_started(&mut self, ctx: &mut Context<Self>) {
        println!("Service Started");
    }
}

impl Supervised for Fetcher {}

#[derive(Message)]
#[rtype(result = "ConferenceResults")]
pub struct GetConferencesCommand;

impl Handler<GetConferencesCommand> for Fetcher {
    type Result = MessageResult<GetConferencesCommand>;

    fn handle(&mut self, _: GetConferencesCommand, ctx: &mut Self::Context) -> Self::Result {
        error!("Handle for GetConferencesCommand");

        let cl = self.conferences.items.clone();

        return MessageResult(ConferenceResults { items: cl });
    }
}
