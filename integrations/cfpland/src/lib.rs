#[macro_use]
extern crate log;

mod cfpland;
pub use cfpland::{Conference, ConferenceResults, FetcherActor, GetConferencesCommand};
