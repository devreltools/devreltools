#[macro_use]
extern crate log;

use actix::prelude::*;
use std::time::Duration;

mod cfpland;
use crate::cfpland::{ConferenceResults, FetcherActor};

fn main() {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    let system = System::new("devreltools-cfpland");

    error!("Help");

    FetcherActor {
        refresh_every: Duration::from_secs(1),
        conferences: ConferenceResults { items: vec![] },
    }
    .start();

    match system.run() {
        Ok(_) => std::process::exit(0),
        Err(_) => std::process::exit(1),
    };
}
