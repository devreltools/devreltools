import { gql, useQuery, NetworkStatus } from "@apollo/client";
import "twin.macro";

export const allEvents = gql`
  query {
    events {
      name
    }
  }
`;

export const EventList = () => {
  const { loading, error, data } = useQuery(allEvents);

  if (error) return <p>There was an error</p>;
  if (loading) return <div>Loading</div>;

  const { events } = data;

  console.log(events);

  return (
    <div tw="flex flex-col">
      <div tw="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div tw="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
          <table tw="min-w-full divide-y divide-gray-200">
            <thead>
              <tr>
                <th tw="px-6 py-3 bg-gray-500 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">
                  Name
                </th>
                <th tw="px-6 py-3 bg-gray-500 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">
                  End Date
                </th>
                <th tw="px-6 py-3 bg-gray-500 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">
                  Something
                </th>
                <th tw="px-6 py-3 bg-gray-500 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">
                  Role
                </th>
                <th tw="px-6 py-3 bg-gray-500" />
              </tr>
            </thead>
            <tbody>
              {/* Odd row */}
              {events.map((event) => (
                <tr tw="bg-white">
                  <td tw="px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                    {event.name}
                  </td>
                  <td tw="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                    Regional Paradigm Technician
                  </td>
                  <td tw="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                    jane.cooper@example.com
                  </td>
                  <td tw="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                    Admin
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};
