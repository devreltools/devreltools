import { gql, useQuery, NetworkStatus } from "@apollo/client";
import "twin.macro";

export const allEvents = gql`
  query {
    conferences {
      name
      category
      cfpStartDate
      cfpDueDate
      city
      country
      cfpUrl
    }
  }
`;

export const EventList = () => {
  const { loading, error, data } = useQuery(allEvents);

  if (error) return <p>There was an error</p>;
  if (loading) return <div>Loading</div>;

  const { conferences } = data;

  console.log(conferences);

  return (
    <div tw="flex flex-col">
      <div tw="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div tw="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
          <table tw="min-w-full divide-y divide-gray-200">
            <thead>
              <tr>
                <th tw="px-6 py-3 bg-gray-500 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">
                  Name
                </th>
                <th tw="px-6 py-3 bg-gray-500 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">
                  Category
                </th>
                <th tw="px-6 py-3 bg-gray-500 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">
                  Dates
                </th>
                <th tw="px-6 py-3 bg-gray-500 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">
                  Location
                </th>
                <th tw="px-6 py-3 bg-gray-500 text-left text-xs leading-4 font-medium text-white uppercase tracking-wider">
                  Url
                </th>
              </tr>
            </thead>
            <tbody>
              {/* Odd row */}
              {conferences.map((event) => (
                <tr tw="bg-white">
                  <td tw="px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900">
                    {event.name}
                  </td>
                  <td tw="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                    {event.category}
                  </td>
                  <td tw="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                    {event.cfpStartDate} - {event.cfpDueDate}
                  </td>
                  <td tw="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                    {event.city}, {event.country}
                  </td>
                  <td tw="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-gray-500">
                    <a target="_blank" href={`${event.cfpUrl}`}>
                      Link
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};
