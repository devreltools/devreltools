import React from "react";
import "tailwindcss/dist/base.min.css";
import { ApolloProvider } from "@apollo/client";
import { useApollo } from "../lib/apollo-client";

import { Layout } from "../components/layout";

export default function App({ Component, pageProps }) {
  const apolloClient = useApollo(pageProps.initialApolloState);

  return (
    <ApolloProvider client={apolloClient}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ApolloProvider>
  );
}
