import { initializeApollo } from "../lib/apollo-client";
import { allEvents, EventList } from "../components/events";
import {
  allEvents as allCfps,
  EventList as CfpList,
} from "../components/call-for-papers";
import "twin.macro";

const IndexPage = () => (
  <>
    <h2 tw="font-bold text-2xl">Events</h2>
    <EventList />

    <h2 tw="font-bold text-2xl mt-32">Call for Papers</h2>
    <CfpList />
  </>
);

export async function getStaticProps() {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: allEvents,
  });

  await apolloClient.query({
    query: allCfps,
  });

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
    revalidate: 1,
  };
}

export default IndexPage;
